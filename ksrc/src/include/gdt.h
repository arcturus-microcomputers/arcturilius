#ifndef GDT_H
#define GDT_H

#include <stdint.h>

// Define a GDT entry
struct gdt_entry {
    uint16_t limit_low;   // Lower 16 bits of the limit
    uint16_t base_low;    // Lower 16 bits of the base
    uint8_t base_middle;  // Next 8 bits of the base
    uint8_t access;       // Access flags
    uint8_t granularity;  // Granularity and limit
    uint8_t base_high;    // Last 8 bits of the base
} __attribute__((packed));

// Define a GDT pointer
struct gdt_ptr {
    uint16_t limit;       // Upper 16 bits of all limits
    uint64_t base;        // Address of the first gdt_entry struct
} __attribute__((packed));

void gdt_install(void);

#endif // GDT_H
