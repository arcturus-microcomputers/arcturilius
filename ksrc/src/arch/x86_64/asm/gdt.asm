section .text
global gdt_flush

gdt_flush:
    lgdt [rdi]
    mov ax, 0x10         ; Data segment selector
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    mov rax, 0x08        ; Code segment selector
    push rax
    lea rax, [rel flush_done] ; Obtain the address of flush_done relative to RIP
    push rax
    retfq

flush_done:
    ret