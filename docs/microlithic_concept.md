# Microlithic Kernel concept

![](img/mlk.png)

All **Modules** are not like those in modular kernels; they are Micro Services, similar to those in **microkernels**. However, the OS can still crash if one service fails, which is why it's called a **Microlithic Kernel**.