- Build System
    - [ ] Make own bs

- Bootloader
    - [ ] Switch to easyboot

- Userspace
    - [ ] Create AGUI (Arctulius Graphical User Interface)
    - [ ] Use glibc/musl instead of mlibc
    - [ ] Switch from Astral Init System to OpenRC